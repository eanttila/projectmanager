import * as React from "react"
import "./styles.css"
import {Navigate, useRoutes} from 'react-router-dom';
import { Main } from "./components/Main"
import { ProjectDetails } from "./components/ProjectDetails"
import { MainLayout } from "./components/MainLayout"
import Modal from "react-modal"

Modal.setAppElement("#root");

const App: React.FC = () => {

  const mainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
      {path: '/*', element: <Navigate to='/' />},
      {path: '/', element: <Main/>},
      {path: '/projects/', element: <Navigate to='/' />},
      {path: '/projects/:id', element: <ProjectDetails/>},
    ],
  };

  const routing = useRoutes([mainRoutes]);

  return <>{routing}</>;
}

export default App