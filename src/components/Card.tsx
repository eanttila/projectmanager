import React from "react";
import { selectTask } from "../store/actionCreators"
import { Dispatch } from "redux"
import { useDispatch } from "react-redux"

interface CardProps {
  task: Task;
}

const Card = React.forwardRef<HTMLDivElement, CardProps>(({ task }, ref) => {
  const dispatch: Dispatch<any> = useDispatch()

  const selectTaskToUpdate = React.useCallback(
    (task: any) => dispatch(selectTask(task)),
    [dispatch, selectTask]
  )

  const handleDragStart = React.useCallback(
    (event: React.DragEvent<HTMLDivElement>) => {
      event.dataTransfer.setData("application/task-id", task.id);
    },
    [task.id]
  );
  return (
    <div className="card" draggable onDragStart={handleDragStart} ref={ref} onClick={() => selectTaskToUpdate(task)}>
      <div className="content">
        {task.name}
        &nbsp;
        <small>{/*task.position*/}</small>
      </div>
    </div>
  );
});

export default Card;