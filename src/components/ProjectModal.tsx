import * as React from "react"
import { Dispatch } from "redux"
import { useDispatch } from "react-redux"
import Modal from "react-modal"
import { deselectProject, updateAndSaveProject, removeProject } from "../store/actionCreators"

Modal.setAppElement("#root");

type Props = {
  selectedProject: {
    name: string,
    description: string,
    boards: any,
    tasks: any
  }
}

export const ProjectModal: React.FC<Props> = ({ selectedProject }) => {
  React.useEffect(() => {
    if (selectedProject) {
      setUpdatedName(selectedProject.name);
      setUpdatedDescription(selectedProject.description);
      setBoards(JSON.parse(selectedProject.boards));
    } else {
      setUpdatedName('');
      setUpdatedDescription('');
      setBoards([]);
    }
  }, [selectedProject]);

  const [updatedName, setUpdatedName] = React.useState('');
  const [updatedDescription, setUpdatedDescription] = React.useState('');
  const [boardName, setBoardName] = React.useState('');
  const [boards, setBoards]: any = React.useState([]);

  function toggleModal() {
    setIsOpen(!isOpen);
  }

  const dispatch: Dispatch<any> = useDispatch();

  const [isOpen, setIsOpen] = React.useState(false);

  const handleBoardName = (e: React.FormEvent<HTMLInputElement>) => {
    setBoardName(e.currentTarget.value)
  }

  const addNewBoard = (e: React.FormEvent) => {
    e.preventDefault();
    if (boards.includes(boardName)) {
      alert('Already added!');
      return;
    }
    if (boardName === '') {
      alert('Give the board a name!');
      return;
    }
    if (boards.length > 8) {
      alert('Too many boards!');
      return;
    }
    setBoards([...boards, boardName]);
    //setBoards((boards: any) => [...boards, boardName])
  }

  const removeBoard = (boardName: string) => {
    //console.log(boardName);
    const updatedBoards: string[] = boards.filter(
      (board: string) => board !== boardName
    );
    setBoards(updatedBoards);
  }

  const updateNewProject = React.useCallback(() => {
    const updatedNewProject = {
      name: updatedName,
      description: updatedDescription,
      boards: boards
    }
    console.log(updatedName);
    console.log(updatedDescription);
    dispatch(updateAndSaveProject(updatedNewProject));
  },
    [dispatch, updateAndSaveProject, updatedName, updatedDescription, boards]
  )

  const deselectProjectToUpdate = React.useCallback(
    () => dispatch(deselectProject()),
    [dispatch, deselectProject]
  )

  const deleteProject = React.useCallback((project: any) => {
    setIsOpen(false);
    dispatch(removeProject(project));
  },
    [dispatch, removeProject]
  )

  const isOpenBoolean: boolean = selectedProject ? true : false
  
  return (
    <div>
      <Modal
        isOpen={isOpenBoolean}
        onRequestClose={() => deselectProjectToUpdate()}
        contentLabel="My dialog"
        className="mymodal"
        overlayClassName="myoverlay"
        closeTimeoutMS={500}
      >
        <div style={{padding: '10px', marginBottom: '35px'}}>
        <div>Edit Project</div>
        <form className="Add-project">
          <b>Name</b>
          <input
            style={{marginTop: '0px'}}
            type="text"
            id="name"
            defaultValue={updatedName}
            placeholder="Name"
            onChange={(e) => setUpdatedName(e.target.value)}
          />
          <b>Description</b>
          <input
            style={{marginTop: '0px'}}
            type="text"
            id="description"
            defaultValue={updatedDescription}
            placeholder="Description"
            onChange={(e) => setUpdatedDescription(e.target.value)}
          />
          <input
          type="text"
          id="boardName"
          value={boardName}
          placeholder="boardName"
          onChange={handleBoardName}
          />
          {selectedProject && selectedProject.tasks.length < 1 ? <div>
            Boards (click to remove):
          <div style={{display: 'flex'}}>
            {selectedProject && boards ? boards.map((board: any, index: number) => {
            return (
              <div
              key={index}
              style={{padding: '10px', marginRight: '5px', border: '1px solid lightBlue', borderRadius: '5px'}}
              onClick={(e) => removeBoard(board)}>
              {board}
            </div>
            );
          }) : null}
          </div>
          <button style={{maxWidth: '120px', marginBottom: '10px', marginTop: '5px', borderRadius: '5px'}} onClick={(e) => addNewBoard(e)}>Add New board</button>
        </div> : <div>Remove current tasks to modify boards!</div> }
        </form>
        <button style={boards.length < 1 ? {float: 'left', marginRight: '5px', background: 'gray', cursor: 'no-drop'} : {float: 'left', marginRight: '5px'}}
        disabled={boards.length < 1 ? true : false}
        onClick={updateNewProject}>
          Update
        </button>
        <button onClick={toggleModal}>Delete</button>
        <button style={{float: 'right'}}onClick={() => deselectProjectToUpdate()}>Close modal</button>
        </div>
          <Modal
          isOpen={isOpen}
          onRequestClose={toggleModal}
          contentLabel="Confirm delete"
          className="mymodal"
          overlayClassName="myoverlay"
          closeTimeoutMS={100}
        >
          <div style={{padding: '10px', marginBottom: '35px'}}>
          <div>Delete?</div>
          <button style={{float: 'left'}}onClick={() => deleteProject(selectedProject)}>Delete</button>
          <button onClick={toggleModal}>Cancel</button>
          </div>
        </Modal>
      </Modal>
    </div>
  )
}