import * as React from "react"
import "../styles.css"
import { Dispatch } from "redux"
import {Link, Outlet} from 'react-router-dom';
import Modal from "react-modal"
import { fetchProjects } from "../store/actionCreators"
import { useDispatch } from "react-redux"

Modal.setAppElement("#root");

export const MainLayout: React.FC = () => {
  React.useEffect(() => {
    fetchNewProjects();
  }, []);

  const dispatch: Dispatch<any> = useDispatch()

  const fetchNewProjects = React.useCallback(
    () => dispatch(fetchProjects()),
    [dispatch]
  )

  return (
    <main>
      <nav>
        <ul>
          {/*<li><Link to='/'>Main Page</Link></li>*/}
          {/*<li><Link to='/projects/1'>projects/1</Link></li>*/}
        </ul>
      </nav>
      <Outlet />
    </main>
  );
}

export default MainLayout