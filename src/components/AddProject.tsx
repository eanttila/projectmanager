import { redirectTo } from "@reach/router";
import * as React from "react"

type Props = {
  fetchProjects: () => void
  saveProject: (project: Project | any) => void
}

export const AddProject: React.FC<Props> = ({ fetchProjects, saveProject }) => {
  const [project, setProject] = React.useState<Project | {}>()
  const [boardName, setBoardName] = React.useState('');
  const [boards, setBoards]: any = React.useState(['todo', 'doing', 'finished']);

  const handleProjectData = (e: React.FormEvent<HTMLInputElement>) => {
    setProject({
      ...project,
      [e.currentTarget.id]: e.currentTarget.value,
    })
  }

  const handleBoardName = (e: React.FormEvent<HTMLInputElement>) => {
    setBoardName(e.currentTarget.value)
  }

  const addNewBoard = (e: React.FormEvent) => {
    e.preventDefault();
    if (boards.includes(boardName)) {
      alert('Already added!');
      return;
    }
    if (boardName === '') {
      alert('Give the board a name!');
      return;
    }
    if (boards.length > 8) {
      alert('Too many boards!');
      return;
    }
    setBoards([...boards, boardName]);
    //setBoards((boards: any) => [...boards, boardName])
  }

  const removeBoard = (boardName: string) => {
    //console.log(boardName);
    const updatedBoards: string[] = boards.filter(
      (board: string) => board !== boardName
    );
    setBoards(updatedBoards);
  }

  const fetchNewProjects = () => {
    fetchProjects();
  }

  const addNewProject = (e: React.FormEvent) => {
    e.preventDefault();
    const newProject = {...project, boards};
    saveProject(newProject);
  }

  return (
    <>
    {/*<button onClick={() => fetchNewProjects()}>Fetch</button>*/}
    <form onSubmit={addNewProject} className="Add-project">
      <input
        type="text"
        id="name"
        placeholder="Name"
        onChange={handleProjectData}
      />
      <input
        type="text"
        id="description"
        placeholder="Description"
        onChange={handleProjectData}
      />
      <input
        type="text"
        id="boardName"
        value={boardName}
        placeholder="boardName"
        onChange={handleBoardName}
      />
      Boards (click to remove):
      <div style={{display: 'flex'}}>
      {boards.map((board: any, index: number) => {
            return (
              <div
              key={index}
              style={{cursor: 'pointer', padding: '10px', marginRight: '5px', border: '2px solid lightBlue', borderRadius: '5px'}}
              onClick={(e) => removeBoard(board)}>
              {board}
            </div>
            );
          })}
      </div>
      <button style={{maxWidth: '120px', marginBottom: '10px', marginTop: '5px', borderRadius: '5px'}} onClick={(e) => addNewBoard(e)}>Add New board</button>
      <button disabled={(project === undefined) || (boards.length < 1) ? true : false}>
        Add project
      </button>
    </form>
    <h3 style={{color: 'black'}}>Projects:</h3>
    </>
  )
}