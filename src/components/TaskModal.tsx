import * as React from "react"
import { Dispatch } from "redux"
import { useDispatch } from "react-redux"
import Modal from "react-modal"
import { deselectTask, updateAndSaveTask, removeTask } from "../store/actionCreators"

Modal.setAppElement("#root");

type Props = {
  selectedTask: {
    id: string
    name: string
    description: string
    state: string,
    position: number,
    projectId: number
  },
  boards: string[]
}

export const TaskModal: React.FC<Props> = ({ selectedTask, boards }) => {
  React.useEffect(() => {
    if (selectedTask) {
      setUpdatedName(selectedTask.name);
      setUpdatedDescription(selectedTask.description);
      setUpdatedState(selectedTask.state);
    } else {
      setUpdatedName('');
      setUpdatedDescription('');
      setUpdatedState('');
    }
  }, [selectedTask]);

  const [updatedName, setUpdatedName] = React.useState('');
  const [updatedDescription, setUpdatedDescription] = React.useState('');
  const [updatedState, setUpdatedState] = React.useState('');

  function toggleModal() {
    setIsOpen(!isOpen);
  }

  const dispatch: Dispatch<any> = useDispatch();

  const [isOpen, setIsOpen] = React.useState(false);

  const updateNewTask = React.useCallback(() => {
    const updatedNewTask = {
      id: selectedTask.id,
      name: updatedName,
      description: updatedDescription,
      state: updatedState,
      position: selectedTask.position,
      projectId: selectedTask.projectId
    }
    dispatch(updateAndSaveTask([updatedNewTask]));
  },
    [dispatch, updateAndSaveTask, updatedName, updatedDescription, updatedState]
  )

  const deselectTaskToUpdate = React.useCallback(
    () => dispatch(deselectTask()),
    [dispatch, deselectTask]
  )

  const deleteTask = React.useCallback((task: any) => {
    setIsOpen(false);
    dispatch(removeTask(task));
  },
    [dispatch, removeTask]
  )

  const isOpenBoolean: boolean = selectedTask ? true : false
  
  return (
    <div>
      <Modal
        isOpen={isOpenBoolean}
        onRequestClose={() => deselectTaskToUpdate()}
        contentLabel="My dialog"
        className="mymodal"
        overlayClassName="myoverlay"
        closeTimeoutMS={500}
      >
        <div style={{padding: '10px', marginBottom: '35px'}}>
        <div>Edit Task</div>
        <form className="Add-project">
          <b>Name</b>
          <input
            style={{marginTop: '0px'}}
            type="text"
            id="name"
            defaultValue={updatedName}
            placeholder="Name"
            onChange={(e) => setUpdatedName(e.target.value)}
          />
          <b>Description</b>
          <input
            style={{marginTop: '0px'}}
            type="text"
            id="description"
            defaultValue={updatedDescription}
            placeholder="Description"
            onChange={(e) => setUpdatedDescription(e.target.value)}
          />
          <b>Importance (board):</b>
          <select
          defaultValue={selectedTask && selectedTask.state}
          onChange={(e) => setUpdatedState(e.target.value)}>
            {boards.map((board: string, index: number) => {
            return (
              <option value={board} key={board}>{board}</option>
            );
          })}
          </select>
        </form>

        <button style={{float: 'left', marginRight: '5px'}} onClick={updateNewTask}>Update task</button>
        <button onClick={toggleModal}>Delete</button>
        <button style={{float: 'right'}}onClick={() => deselectTaskToUpdate()}>Close modal</button>
        </div>
          <Modal
          isOpen={isOpen}
          onRequestClose={toggleModal}
          contentLabel="Confirm delete"
          className="mymodal"
          overlayClassName="myoverlay"
          closeTimeoutMS={100}
        >
          <div style={{padding: '10px', marginBottom: '35px'}}>
          <div>Delete?</div>
          <button style={{float: 'left'}}onClick={() => deleteTask(selectedTask)}>Delete</button>
          <button onClick={toggleModal}>Cancel</button>
          </div>
        </Modal>
      </Modal>
    </div>
  )
}