interface Task {
  id: string;
  name: string;
  state: string;
  position: number;
}

export function getNewPositionValue(
  tasks: readonly Task[],
  previousTaskId: string | null,
  nextTaskId: string | null
) {
  let newPos = Math.random() * 10000;
  // Get the tasks in the new column, in position order.
  const sTasks = [...tasks].sort((a, b) => a.position - b.position);
  if (sTasks.length) {
    // Find the previous/next tasks, if any.
    const prevTask = sTasks.find((t) => t.id === previousTaskId);
    const nextTask = sTasks.find((t) => t.id === nextTaskId);
    // If there is no next task, find the last position in the list and add 1.
    if (!nextTask) newPos = sTasks[sTasks.length - 1].position + 1;
    // If there is no previous task, find the first position in the list and subtract 1.
    else if (!prevTask) newPos = sTasks[0].position - 1;
    else {
      // Otherwise, squeeze the task in between.
      newPos = (prevTask?.position || 0 + nextTask?.position || 0) / 2;
    }
  }
  return newPos;
}

export function getPreviousAndNextCards(
  cards: Record<string, HTMLDivElement | null>,
  eventY: number
): [string | null, string | null] {
  let previousTaskId: string | null = null;
  let nextTaskId: string | null = null;
  const currentCardYs: [string, number][] = [];
  Object.entries(cards).forEach(([id, div]) => {
    if (div) {
      const rect = div.getBoundingClientRect();
      const midY = (rect.top + rect.bottom) / 2;
      currentCardYs.push([id, midY]);
    }
  });
  currentCardYs.sort((a, b) => a[1] - b[1]);
  // TODO: I'm pretty sure there's a really simple way to figure out
  //       the insertion point, and this isn't it
  for (let i = 0; i < currentCardYs.length - 1; i++) {
    const [id1, y1] = currentCardYs[i];
    const [id2, y2] = currentCardYs[i + 1];
    if (eventY >= y1) {
      previousTaskId = id1;
    }
    if (eventY >= y1 && eventY <= y2) {
      previousTaskId = id1;
      nextTaskId = id2;
      break;
    }
  }
  if (previousTaskId === null && nextTaskId === null && currentCardYs.length) {
    nextTaskId = currentCardYs[0][0];
  }
  return [previousTaskId, nextTaskId];
}
