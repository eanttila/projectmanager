import * as React from "react";
import "../kanban.css";
import { getNewPositionValue } from "./utils";
import Dropzone from "./Dropzone";
import { Dispatch } from "redux"
import { useDispatch } from "react-redux"
import { updateAndSaveTask } from "../store/actionCreators"

const states = [
  "todo",
  "inprogress",
  "testing",
  "done"
];

type Props = {
  newTasks: Task[],
  projectBoards: string[]
}

export const Kanban: React.FC<Props> = ({ newTasks, projectBoards }) => {
  React.useEffect(() => {
    setTasks(newTasks);
  }, [newTasks, projectBoards]);
  const dispatch: Dispatch<any> = useDispatch();
  const [tasks, setTasks] = React.useState<Task[]>(() => []);

  const handleCardDrag = React.useCallback(
    (
      cardId: string,
      state: string,
      previousTaskId: string | null,
      nextTaskId: string | null
    ) => {
      setTasks((tasks) => {
        // Find the new position value among the other cards in this zone.
        const position = getNewPositionValue(
          tasks.filter((t) => t.state === state),
          previousTaskId,
          nextTaskId
        );
        // Modify the single card.
        newTasks = tasks.map((t) =>
          t.id === cardId ? { ...t, state, position } : t
        );
        console.log(newTasks);
        dispatch(updateAndSaveTask(newTasks));
        return newTasks;
      });
    },
    []
  );

  if (!projectBoards) {
    return (
      <div className={"Kanban"}>
        Loading
      </div>
    );
  }

  return (
    <div className={"Kanban"}>
      {projectBoards.map((s) => (
        <Dropzone
          onCardDragged={handleCardDrag}
          state={s}
          cards={tasks.filter((t) => t.state === s)}
          key={s}
        />
      ))}
    </div>
  );
}
