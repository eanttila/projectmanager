import * as React from "react"
import { useSelector, shallowEqual, useDispatch } from "react-redux"
import "../styles.css"
import { Project } from "./Project"
import { AddProject } from "./AddProject"
import { fetchProjects, addProject, selectProject, deselectProject, removeProject } from "../store/actionCreators"
import { Dispatch } from "redux"
import Modal from "react-modal"
import { ProjectModal } from "./ProjectModal"

Modal.setAppElement("#root");

export const Main: React.FC = () => {
  React.useEffect(() => {
    fetchNewProjects();
  }, []);

  const projects: readonly Project[] = useSelector(
    (state: ProjectState) => state.projects,
    shallowEqual
  )

  const dispatch: Dispatch<any> = useDispatch()

  const fetchNewProjects = React.useCallback(
    () => dispatch(fetchProjects()),
    [dispatch]
  )

  const saveProject = React.useCallback(
    (project: Project) => dispatch(addProject(project)),
    [dispatch]
  )

  const selectedProject: Project | any = useSelector(
    (state: ProjectState) => state.selectedProject,
    shallowEqual
  )

  return (
    <>
      <h1>Project manager with Kanban for tasks</h1>
      <AddProject fetchProjects={fetchNewProjects} saveProject={saveProject} />
      {projects.map((project: Project) => (
        <Project
          key={project.id}
          project={project}
          selectProject={selectProject}
          deselectProject={deselectProject}
          removeProject={removeProject}
        />        
      ))}
      <ProjectModal selectedProject={selectedProject} />
    </>
  )
}

export default Main