import * as React from "react"
import { Dispatch } from "redux"
import { useSelector, shallowEqual, useDispatch } from "react-redux"
import {useParams} from 'react-router-dom';
import Modal from "react-modal"
import { fetchProjectDetails } from "../store/actionCreators"
import {Link} from 'react-router-dom';
import { ProjectModal } from "./ProjectModal"
import { TaskModal } from "./TaskModal"
import { Kanban } from "./Kanban"
import { selectProject, selectTask, addTask } from "../store/actionCreators"

Modal.setAppElement("#root");

export const ProjectDetails: React.FC = () => {
  React.useEffect(() => {
    fetchNewProjectDetails(params.id);
  }, []);

  const [task, setTask] = React.useState<Task | {}>()

  const selectedProject: Project | any = useSelector(
    (state: ProjectState) => state.selectedProject,
    shallowEqual
  )

  const selectedTask: Project | any = useSelector(
    (state: ProjectState) => state.selectedTask,
    shallowEqual
  )

  const dispatch: Dispatch<any> = useDispatch()
  const params = useParams();
  
  const fetchNewProjectDetails = React.useCallback(
    (id: string) => dispatch(fetchProjectDetails(params.id)),
    [dispatch]
  )

  const fetchedProject: Project | any = useSelector(
    (state: ProjectState) => state.fetchedProject,
    shallowEqual
  )

  const selectProjectToUpdate = React.useCallback(
    (project: any) => dispatch(selectProject(project)),
    [dispatch, selectProject]
  )

  const selectTaskToUpdate = React.useCallback(
    (task: any) => dispatch(selectTask(task)),
    [dispatch, selectTask]
  )

  const handleTaskData = (e: React.FormEvent<HTMLInputElement>) => {
    setTask({
      ...task,
      [e.currentTarget.id]: e.currentTarget.value,
    })
  }

  const saveTask = React.useCallback((task: any) => {
    dispatch(addTask(task));
  },
    [dispatch, addTask]
  )

  const addNewTask = (e: React.FormEvent) => {
    e.preventDefault()
    saveTask(task)
  }
  
  if (!fetchedProject || !fetchedProject.tasks) {
    return (
      <div className="ProjectDetails">
        Loading...
      </div>
    )
  }
  
  return (
    <div className="ProjectDetails">
      <button style={{marginBottom: '10px'}} onClick={() => selectProjectToUpdate(fetchedProject)}>Update project</button>
      <table>
      <tbody>
      <tr>
        <th>Project ID:</th>
        <th>Name:</th>
        <th>Description:</th>
      </tr>
      <tr>
      <td>{fetchedProject.id}</td>
      <td>{fetchedProject.name}</td>
      <td>{fetchedProject.description}</td>
    </tr>
      </tbody>
      </table>
      <form onSubmit={addNewTask} className="Add-project">
      <input
        type="text"
        id="name"
        placeholder="Name"
        onChange={handleTaskData}
      />
      <input
        type="text"
        id="description"
        placeholder="Description"
        onChange={handleTaskData}
      />
      <button disabled={task === undefined ? true : false}>
        Add task
      </button>
    </form>
      {/*fetchedProject.tasks.map((task: Task) => {
          return (
            <div key={task.id}>
            <p>{task.name}</p>
            <p>{task.description}</p>
            <button style={{marginBottom: '10px'}} onClick={() => selectTaskToUpdate(task)}>Update project</button>
          </div>
          );
        })*/}
      {/*`View Account ID "${params.id}"`*/}
      <Link style={{color: 'white'}} to={`/projectmanager/`}><button style={{marginBottom: '2px'}}>Back to projects</button></Link>
      <h2 style={{marginTop: '10px'}}>Tasks under project:</h2>
      <small>Click task to open it!</small>
      <Kanban newTasks={fetchedProject.tasks} projectBoards={JSON.parse(fetchedProject.boards)} />
      <ProjectModal selectedProject={selectedProject} />
      <TaskModal selectedTask={selectedTask} boards={fetchedProject ? JSON.parse(fetchedProject.boards) : []} />
      {/*JSON.parse(fetchedProject.boards).map((board: any, index: number) => {
          return (
            <div key={index}>
            {board}
          </div>
          );
        })*/}
    </div>
  )
}