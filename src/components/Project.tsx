import * as React from "react"
import { Dispatch } from "redux"
import { useSelector, shallowEqual, useDispatch } from "react-redux"
import Modal from "react-modal"
import {Link} from 'react-router-dom';

Modal.setAppElement("#root");

type Props = {
  project: Project
  selectProject: (project: Project) => void
  deselectProject: (project: Project) => void
  removeProject: (project: Project) => void  
}

export const Project: React.FC<Props> = ({ project, selectProject }) => {
  const dispatch: Dispatch<any> = useDispatch()

  const selectProjectToUpdate = React.useCallback(
    (project: any) => dispatch(selectProject(project)),
    [dispatch, selectProject]
  )

  return (
    <div className="Project" style={{border: '1px solid black', margin: '5px', boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px'}}>
      <div style={{display: 'grid', paddingRight: '10px'}}>
      <Link style={{color: 'white'}} to={`projects/${project.id}`}><button style={{marginBottom: '2px'}}>Open</button></Link>
      <button onClick={() => selectProjectToUpdate(project)}>Update</button>
      </div>
      <div>
        <h1>{project.name}</h1>
        <p>{project.description}</p>
        <b>ID: {project.id}</b>        
      </div>
    </div>
  )
}