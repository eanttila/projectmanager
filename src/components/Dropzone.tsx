import * as React from "react";
import { getPreviousAndNextCards } from "./utils";
import Card from "./Card";
import { generateKeyPair } from "crypto";

interface DropzoneProps {
  cards: ReadonlyArray<Task>;
  state: string;
  onCardDragged: (
    taskId: string,
    targetZone: string,
    previousTaskId: string | null,
    nextTaskId: string | null
  ) => void;
}

export default function Dropzone({
  cards,
  state,
  onCardDragged
}: DropzoneProps) {
  /*
  let stateName = '';
  if (state === 'todo') {
    stateName = 'Todo';
  } else if (state === 'inprogress') {
    stateName = 'In progress';
  } else if (state === 'testing') {
    stateName = 'Testing';
  } else if (state === 'done') {
    stateName = 'Done';
  } else {
    stateName = 'Other';
  }
  */
  const dropzoneDivRef = React.useRef<HTMLDivElement>(null);
  const cardsRefs = React.useRef<Record<string, HTMLDivElement | null>>({});
  const handleDragOver = React.useCallback(
    (event: React.DragEvent<HTMLDivElement>) => {
      if (event.dataTransfer.types.includes("application/task-id")) {
        event.preventDefault(); // allow drop here
      }
    },
    []
  );
  const handleDrop = React.useCallback(
    (event: React.DragEvent<HTMLDivElement>) => {
      const taskId = event.dataTransfer.getData("application/task-id");
      if (!taskId) return;
      const [previousTaskId, nextTaskId] = getPreviousAndNextCards(
        cardsRefs.current,
        event.clientY
      );
      onCardDragged(taskId, state, previousTaskId, nextTaskId);
    },
    [state, onCardDragged]
  );
  return (
    <div
      className="dropzone"
      style={{boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px'}}
      data-dzid={state}
      onDragOver={handleDragOver}
      onDrop={handleDrop}
      ref={dropzoneDivRef}
    >
      <h3>{state}</h3>
      {[...cards]
        .sort((a, b) => a.position - b.position)
        .map((task) => (
          <Card
            task={task}
            key={task.id}
            ref={(div) => (cardsRefs.current[task.id] = div)}
          />
        ))}
    </div>
  );
}
