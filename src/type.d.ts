interface Project {
    id: string
    name: string
    description: string
    boards: string
    tasks: Task[] | null
  }

  interface Task {
    id: string
    name: string
    description: string
    state: string
    position: number
    projectId: number
  }
  
  type ProjectState = {
    modalState: string
    selectedProject: Project | null
    projects: Project[]
    fetchedProject: Project | null
    selectedTask: Task | null
  }

  type ProjectFetchAction = {
    type: string
    projects: Project[]
  }
  
  type ProjectAction = {
    type: string
    project: Project | null
  }

  type TaskAction = {
    type: string
    task: Task | null
  }

  type TasksAction = {
    type: string
    tasks: Task[]
  }