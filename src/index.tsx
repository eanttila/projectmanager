import * as React from "react";
import { render } from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { HashRouter } from 'react-router-dom';

import App from "./App";
import reducer from "./store/reducer";

const store = createStore(
  reducer,
  applyMiddleware(thunk),
);

const rootElement = document.getElementById("root");
render(
  <Provider store={store}>
    <HashRouter>
    <App />
    </HashRouter>
  </Provider>,
  rootElement
);