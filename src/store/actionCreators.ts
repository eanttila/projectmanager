import * as actionTypes from "./actionTypes"

export function fetchProjects() {
  return async (dispatch: any, getState: any) => {
    //const selectedProject = getState().selectedProject;
    const response = await fetch('api/');
    const toJson = await response.json();
    const action: ProjectFetchAction = {
      type: actionTypes.FETCH_PROJECTS,
      projects: toJson,
    }
    dispatch(action)
  }
}

export function fetchProjectDetails(id: string) {
  return async (dispatch: any, getState: any) => {
    try {
    const response = await fetch(`api/getproject.php?id=${id}`);
    const toJson = await response.json();
    const action: ProjectAction = {
      type: actionTypes.SET_FETCHED_PROJECT,
      project: toJson,
    }
    dispatch(action)
    return;
      } catch (e) {
        console.log(e);
        return;
    }
  }
}

export function addProject(project: Project) {
  return async (dispatch: any, getState: any) => {
    const settings = {
      method: 'POST',
      headers: {
      'Accept': 'application/json, text/plain, */*'
      },
      body: JSON.stringify(
				{
          project
				})
    };
    try {
      const fetchResponse = await fetch(`api/postnew.php`, settings);
      const data = await fetchResponse.json();
      const action: ProjectAction = {
        type: actionTypes.ADD_PROJECT,
        project: data,
      }
      dispatch(action)
      return;
    } catch (e) {
        console.log(e);
        return;
    }
  }
}

export function updateAndSaveProject(updatedProject: any) {
  return async (dispatch: any, getState: any) => {
    const selectedProjectId = getState().selectedProject.id;
    updatedProject.id = selectedProjectId;    
    const settings = {
      method: 'POST',
      headers: {
      'Accept': 'application/json, text/plain, */*'
      },
      body: JSON.stringify(
				{
          updatedProject
				})
    };
    try {
      const fetchResponse = await fetch(`api/update.php`, settings);
      const data = await fetchResponse.json();
      const action: ProjectAction = {
        type: actionTypes.UPDATE_PROJECT,
        project: data,
      }
      dispatch(action)
      dispatch(deselectProject());
      return;
    } catch (e) {
        console.log(e);
        return;
    }
  }
}

export function selectProject(project: Project) {
  return async (dispatch: any, getState: any) => {
    const action: ProjectAction = {
      type: actionTypes.SELECT_PROJECT,
      project,
    }
    dispatch(action)
  }
}

  export function deselectProject() {
    return async (dispatch: any, getState: any) => {
      const action: ProjectAction = {
        type: actionTypes.DESELECT_PROJECT,
        project: null,
      }
      dispatch(action)
    }
  }

export function removeProject(project: Project) {
return async (dispatch: any, getState: any) => {
  const settings = {
    method: 'GET'
  };
  try {
    await fetch(`api/delete.php/?id=${project.id}`, settings);
    //const data = await fetchResponse.json();
    const action: ProjectAction = {
      type: actionTypes.REMOVE_PROJECT,
      project,
    }
    dispatch(action)
    return;
  } catch (e) {
      console.log(e);
      return;
  }
}
}

export function addTask(task: Task) {
  return async (dispatch: any, getState: any) => {
    task.projectId = getState().fetchedProject.id;
    const firstBoard = JSON.parse(getState().fetchedProject.boards);
    const settings = {
      method: 'POST',
      headers: {
      'Accept': 'application/json, text/plain, */*'
      },
      body: JSON.stringify(
				{
          task,
          firstBoard: firstBoard[0]
				})
    };
    try {
      const fetchResponse = await fetch(`api/postnewtask.php`, settings);
      const data = await fetchResponse.json();
      const action: TaskAction = {
        type: actionTypes.ADD_TASK,
        task: data,
      }
      dispatch(action)
      return;
    } catch (e) {
        console.log(e);
        return;
    }
  }
}

export function selectTask(task: Task) {
  return async (dispatch: any, getState: any) => {
    const action: TaskAction = {
      type: actionTypes.SELECT_TASK,
      task,
    }
    dispatch(action)
  }
}

  export function deselectTask() {
    return async (dispatch: any, getState: any) => {
      const action: TaskAction = {
        type: actionTypes.DESELECT_TASK,
        task: null,
      }
      dispatch(action)
    }
  }

  export function updateAndSaveTask(updatedTasks: Task[]) {
    return async (dispatch: any, getState: any) => {
      console.log(updatedTasks);
      const fetchedProjectId = getState().fetchedProject.id;
      //boards[0]
      const settings = {
        method: 'POST',
        headers: {
        'Accept': 'application/json, text/plain, */*'
        },
        body: JSON.stringify(
          {
            tasks: updatedTasks,
            fetchedProjectId: fetchedProjectId
          })
      };
      try {
        const fetchResponse = await fetch(`api/updatetask.php`, settings);
        const data = await fetchResponse.json();
        const action: TasksAction = {
          type: actionTypes.UPDATE_TASK,
          tasks: data,
        }
        //console.log(data);
        dispatch(action)
        dispatch(deselectTask());
        return;
      } catch (e) {
          console.log(e);
          return;
      }
    }
  }

  export function removeTask(task: Task) {
  return async (dispatch: any, getState: any) => {
    const allTasks = getState().fetchedProject.tasks;
    const settings = {
      method: 'GET'
    };
    try {
      await fetch(`api/deletetask.php/?id=${task.id}`, settings);
      //const data = await fetchResponse.json();
      const action: TaskAction = {
        type: actionTypes.REMOVE_TASK,
        task,
      }
      const selectedBoardTasks: Task[] = allTasks.filter(
        (t: Task) => (t.state === task.state && t.id !== task.id)
      );
      selectedBoardTasks.map((element, index) => {
        element.position = index;
        return element;
      });
      dispatch(updateAndSaveTask(selectedBoardTasks));
      dispatch(action)
      return;
    } catch (e) {
        console.log(e);
        return;
    }
  }
  }

  export function simulateHttpRequest(action: any) {
    console.log('simulateHttpRequest');
  return (dispatch: any, getState: any) => {
    setTimeout(() => {
      console.log('setTimeout');
      dispatch(action)
    }, 500)
  }

}