import * as actionTypes from "./actionTypes";

const initialState: ProjectState = {
  modalState: 'is-active',
  selectedProject: null,
  selectedTask: null,
  projects: [],
  fetchedProject: null
};

const reducer = (
  state: ProjectState = initialState,
  action: any
): ProjectState => {
  //console.log(action.type);
  switch (action.type) {
    case actionTypes.FETCH_PROJECTS:
      return {
        ...state,
        projects: action.projects
      };
    case actionTypes.SET_FETCHED_PROJECT:
      return {
        ...state,
        fetchedProject: action.project
      };
    case actionTypes.ADD_PROJECT:
      const newProject: Project = {
        id: action.project.id,
        name: action.project.name,
        description: action.project.description,
        boards: action.project.boards,
        tasks: []
      };
      return {
        ...state,
        projects: state.projects.concat(newProject)
      };
    case actionTypes.UPDATE_PROJECT:
      const updatedProjectsAfterUpdate: Project[] = state.projects.filter(
        (project) => project.id !== action.project.id
      );
      const newUpdatedProjectsAfterUpdate = updatedProjectsAfterUpdate.concat(action.project);
      const newUpdatedProjectsAfterUpdateSorted = newUpdatedProjectsAfterUpdate.sort((a, b) => Number(a.id) - Number(b.id));
      const newFetchedProjectAfterUpdate = action.project;
      newFetchedProjectAfterUpdate.boards = JSON.stringify(newFetchedProjectAfterUpdate.boards);
      return {
        ...state,
        projects: newUpdatedProjectsAfterUpdateSorted,
        fetchedProject: newFetchedProjectAfterUpdate
      };
    case actionTypes.SELECT_PROJECT:
        return {
          ...state,
          selectedProject: action.project
        };
    case actionTypes.DESELECT_PROJECT:
      return {
        ...state,
        selectedProject: null
      };
    case actionTypes.REMOVE_PROJECT:
      const updatedProjects: Project[] = state.projects.filter(
        (project) => project.id !== action.project.id
      );
      return {
        ...state,
        selectedProject: null,
        projects: updatedProjects
      };
    case actionTypes.ADD_TASK:
      if (state.fetchedProject && state.fetchedProject.tasks) {
        const newTasks = state.fetchedProject.tasks.concat({...action.task});
        const newUpdatedTasksAfterAddSorted = newTasks.sort((a, b) => Number(a.id) - Number(b.id));
        return {
          ...state,
          fetchedProject: {...state.fetchedProject, tasks: newUpdatedTasksAfterAddSorted}
        };
      } else {
        return {
          ...state,
          fetchedProject: state.fetchedProject
        };
      }
    case actionTypes.SELECT_TASK:
        return {
          ...state,
          selectedTask: action.task
        };
    case actionTypes.DESELECT_TASK:
      return {
        ...state,
        selectedTask: null
      };
    case actionTypes.UPDATE_TASK:
      const newFetchedProject = state.fetchedProject;
     if (newFetchedProject) {
      newFetchedProject.tasks = action.tasks;
     }
      return {
        ...state,
        fetchedProject: newFetchedProject,
        selectedTask: null
      };
    case actionTypes.REMOVE_TASK:
      const newFetchedProjectAfterRemove = state.fetchedProject;
      if (newFetchedProjectAfterRemove && newFetchedProjectAfterRemove.tasks) {
        const newTasks = newFetchedProjectAfterRemove.tasks.filter(
          (task) => task.id !== action.task.id
        );
        const newUpdatedTasksAfterDeleteSorted = newTasks.sort((a, b) => Number(a.id) - Number(b.id));
        newFetchedProjectAfterRemove.tasks = newUpdatedTasksAfterDeleteSorted;
      }
      return {
        ...state,
        selectedTask: null,
        fetchedProject: newFetchedProjectAfterRemove
      };
  }
  return state;
};

export default reducer;
